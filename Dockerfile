FROM jenkins/jenkins:lts
RUN jenkins-plugin-cli --plugins \
 kubernetes \
 workflow-aggregator \
 git \
 git-parameter \
 configuration-as-code \
 ssh-credentials